/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  ListView,
  TouchableHighlight,
  AlertIOS,
} from 'react-native';

export default class AwesomeProject extends Component {

  constructor(props) {
    super(props);

    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {

      movie: "hello"
    };

  }

  render() {
    let movieTitle = this.state.movie ? "hello" : 'fail';

    fetch('https://facebook.github.io/react-native/movies.json')
      .then((response) => response.json())
      .then((responseData) => {
            AlertIOS.alert(
                "GET Response",
                "Search Query -> " + responseData.movies[0].title
            )
      })
      .catch((error) => {
        this.setState({ movie: "fail"});
        console.error(error);
      })
      .done();

    return (
      <View style={{flex: 1, paddingTop: 22}}>
        <Text>
          {movieTitle}
        </Text>

            <View>
                <TouchableHighlight onPress={this._onPressButtonGET}>
                    <Text>GET</Text>
                </TouchableHighlight>
                <TouchableHighlight onPress={this._onPressButtonPOST}>
                    <Text>POST</Text>
                </TouchableHighlight>
            </View>
       
      </View>  
    );
  }
}

 function getMoviesFromApiAsync() {
    return fetch('https://facebook.github.io/react-native/movies.json')
      .then((response) => response.json())
      .then((responseJson) => {
        return responseJson.movies;
      })
      .catch((error) => {
        console.error(error);
      });
  }

const styles = StyleSheet.create({
  bigblue: {
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 30,
  },
  red: {
    color: 'red',
  },
});

class Blink extends Component {
  constructor(props) {
    super(props);
    this.state = {showText: true};

    // Toggle the state every second
    setInterval(() => {
      this.setState({ showText: !this.state.showText });
    }, 1000);
  }

  render() {
    let display = this.state.showText ? this.props.text : ' ';
    return (
      <Text>{display}</Text>
    );
  }
}

class Greeting extends Component {
  render() {
    return (
      <Text>Hello {this.props.name}!</Text>
    );
  }
}

class LotsOfGreetings extends Component {
  render() {
    return (
      <View style={{alignItems: 'center'}}>
        <AwesomeProject name='Rexxar' />
        <AwesomeProject name='Jaina' />
        <AwesomeProject name='Valeera' />
      </View>
    );
  }
}

AppRegistry.registerComponent('LotsOfGreetings', () => LotsOfGreetings);

/*
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
*/

AppRegistry.registerComponent('AwesomeProject', () => AwesomeProject);
