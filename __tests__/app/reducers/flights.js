import * as types from '../../../app/actions/types'
import * as reducers from '../../../app/reducers/flights'

const testSuccessFlightStatusResponseMulti = require('../testResponses/testFlightStatusResponseMultiFlight.json');
const testSuccessFlightStatusResponseSingle = require('../testResponses/testFlightStatusResponseSingle.json');

describe('reducers', () => {

	describe('fetchError reducer', () => {
		describe('set fetch error', () => {
			it('should return the initial state', () => {
		    	expect(
		    		reducers.fetchError(undefined, {})
		    	).toEqual({})
			});

			it('should set error message', () => {
		    	expect(
		    		reducers.fetchError([], {
		    			type: types.SET_FETCH_ERROR,
		    			errorMessage: "This is an error"
		    		})
		    	).toEqual("This is an error")
			});

			it('should not set error message', () => {
		    	expect(
		    		reducers.fetchError([], {
		    			type: types.SET_FETCH_ERROR,
		    		})
		    	).toEqual({})
			});
		});

		describe('clear fetch error', () => {
			it('should clear error message', () => {
		    	expect(
		    		reducers.fetchError([{
		    			errorMessage: "This is an error"
		    		}], {
		    			type: types.CLEAR_FETCH_ERROR,
		    		})
		    	).toEqual({})
			});
		});

	});

	describe('searchedFlights reducer', () => {
		describe('set searched flights', () => {
			it('should return the initial state', () => {
		    	expect(
		    		reducers.searchedFlights(undefined, {})
		    	).toEqual({})
			});

			it('should return the initial state when no flight data', () => {
				let initialState = [{1234 : {
					type: types.SET_SEARCHED_FLIGHTS
				}}];

				expect(
		    		reducers.searchedFlights(initialState, {})
		    	).toEqual(initialState)
			});

			it('should return the initial state when no flight status', () => {
				let initialState = [{1234 : {}}];
		    		
				expect(
		    		reducers.searchedFlights(initialState, {
		    			type: types.SET_SEARCHED_FLIGHTS,
		    			flightData: {}
		    		})
		    	).toEqual(initialState)
			});

			it('should return flight data when single flight status found', () => {	  
				var result = reducers.searchedFlights([], {
						type: types.SET_SEARCHED_FLIGHTS,
		    			flightData: testSuccessFlightStatusResponseSingle
		    		});

				expect(Object.keys(result)).toEqual(expect.arrayContaining(['876140105']));
			});

			it('should return multiple flight data when multiple flight status found', () => {	    		

				var result = reducers.searchedFlights([], {
						type: types.SET_SEARCHED_FLIGHTS,
		    			flightData: testSuccessFlightStatusResponseMulti
		    		});
				
				expect(Object.keys(result)).toEqual(expect.arrayContaining(['876140168', '876635746']));
			});
			
		});

		describe('clear searched flights', () => {

			it('should clear searched flights', () => {
				let initialState = [{1234 : {}}];

				expect(
			    		reducers.searchedFlights(initialState, {
							type: types.CLEAR_SEARCHED_FLIGHTS
						})
			    	).toEqual({})

			});
		});
	});

});