import FlightHelper from '../../../app/lib/flightHelper'

import expect from 'expect' // You can use any testing library

const testSuccessFlightStatusResponseMulti = require('../testResponses/testFlightStatusResponseMultiFlight.json');

describe('flight helper methods', () => {

	describe('isFlightStatsId', () => {
		it('should be true when flightId is a flightStatsId', () => {
			expect(FlightHelper.isFlightStatsId("42901")).toEqual(true);
		});

		it('should be false when flightId is not a flightStatsId', () => {
			expect(FlightHelper.isFlightStatsId("abc019")).toEqual(false);
		});
	});

	describe('parseFlightStatsId', () => {
		it('should be flightStatsId when flightId is a flightStatsId', () => {
			expect(FlightHelper.parseFlightStatsId("42901")).toEqual("42901");
		});

		it('should be null when flightId is not a flightStatsId', () => {
			expect(FlightHelper.parseFlightStatsId("abc019")).toEqual(null);
		});
	});

	describe('isValidFlight', () => {
		it('should be true when flightId is a valid flightId', () => {
			expect(FlightHelper.isValidFlight("AA120")).toEqual(true);
		});

		it('should be false when flightId is not a valid flightId', () => {
			expect(FlightHelper.isValidFlight("129abc")).toEqual(false);
		});
	});

	describe('parseFlightId', () => {
		it('should be flight info when flightId is a valid flightId', () => {

			const expected = {
				airlineCode: "AA",
				flightId: "120"
			}

			expect(FlightHelper.parseFlightId("AA120")).toEqual(expected);
		});

		it('should be null when flightId is not a valid flightId', () => {
			expect(FlightHelper.parseFlightId("129abc")).toEqual({});
		});
	});

	describe('getApiKeyParams', () => {
		it('should be api params string', () => {
			expect(FlightHelper.getApiKeyParams()).toMatch(/appId=[0-9a-zA-Z]+\&appKey=[0-9a-zA-Z]+/);
		});
	});

	describe('getFlightStatusUrl', () => {
		it('should be url path when is valid flightStatsId', () => {
			const expected = "/flex/flightstatus/rest/v2/json/flight/status/13579";
			expect(FlightHelper.getFlightStatusUrl("13579", "2017-04-14")).toEqual(expected);
		});

		it('should be url path when is valid flightId', () => {
			const expected = "/flex/flightstatus/rest/v2/json/flight/status/AA/120/arr/2017/4/13";
			expect(FlightHelper.getFlightStatusUrl("AA120", "2017-04-14")).toEqual(expected);
		});

		it('should be base url path when is invalid flightId', () => {
			const expected = "/flex/flightstatus/rest/v2/json/flight/status/";
			expect(FlightHelper.getFlightStatusUrl("120aa", "2017-04-14")).toEqual(expected);
		});

		it('should be url path with current date when date empty', () => {
			let date = new Date();
			const expected = "/flex/flightstatus/rest/v2/json/flight/status/AA/120/arr/" 
				+ date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();

			expect(FlightHelper.getFlightStatusUrl("AA120", "")).toEqual(expected);
		});
	});

	describe('addAdditionalFlightData', () => {
		var flightStatuses, appendix;

		beforeEach(() => {
			appendix = testSuccessFlightStatusResponseMulti.appendix;
			flightStatuses = testSuccessFlightStatusResponseMulti.flightStatuses;
		});

		it('should be given flight data when appendix is invalid', () => {
			expect(FlightHelper.addAdditionalFlightData(undefined, flightStatuses[0])).toEqual(flightStatuses[0]);
		});

		it('should be undefined when flight info given is invalid', () => {
			expect(FlightHelper.addAdditionalFlightData(appendix, undefined)).toBe(undefined);
		});

		it('should populate flight data with more info when is success', () => {
			var flightResult = FlightHelper.addAdditionalFlightData(appendix, flightStatuses[0]);

			expect(flightResult.flightDisplayName).toNotBe(undefined);
			expect(flightResult.departureAirportDisplay).toNotBe(undefined);
			expect(flightResult.arrivalAirportDisplay).toNotBe(undefined);
			expect(flightResult.departureLocalDateDisplay).toNotBe(undefined);
			expect(flightResult.arrivalLocalDateDisplay).toNotBe(undefined);
		});


	});

});
