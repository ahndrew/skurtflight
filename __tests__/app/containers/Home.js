import React from 'react'
import {shallow} from 'enzyme'

import {mount} from 'enzyme'
import {expect} from 'chai'
import sinon from 'sinon'

import {Home} from '../../../app/containers/Home'

import renderer from 'react-test-renderer';

describe('Home', () => {
	it('should render', ()=> {
		const props = {
	      searchedFlights: {},
	      fetchError: {}
	    }

  		const component = renderer.create(
    		<Home {...props} />
  		);

  		//console.log(component);

  		let tree = component.toJSON();

  		//console.log(tree);

  		expect(tree).toMatchSnapshot();
 	});
});