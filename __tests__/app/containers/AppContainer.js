import React from 'react';
import {shallow} from 'enzyme'

import renderer from 'react-test-renderer';

import {Home} from '../../../app/containers/Home'

describe('components', function() {
  describe('<Home />', function() {
    it('renders correctly', function() {
    	const props = {
	      searchedFlights: {},
	      fetchError: {}
	    };

      	var tree = shallow(<Home {...props} />);
      	expect(tree).toMatchSnapshot();
    });
  });
});