import * as actions from '../../../app/actions/flights'
import * as types from '../../../app/actions/types'

import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import expect from 'expect' // You can use any testing library

const middlewares = [ thunk ]
const mockStore = configureMockStore(middlewares)

const testSuccessFlightStatusResponseMulti = require('../testResponses/testFlightStatusResponseMultiFlight.json');
const testSuccessFlightStatusResponseSingle = require('../testResponses/testFlightStatusResponseSingle.json');

describe('async actions', () => {

  describe('clearFlights', () => {
    it('creates SET_CLEAR_FLIGHTS when clearFlights is done', () => {
      const expectedActions = [
        { type: types.CLEAR_SEARCHED_FLIGHTS },
      ]
      const store = mockStore({ searchedFlights: {} })

      return store.dispatch(actions.clearFlights())
        .then(() => { // return of async actions
          expect(store.getActions()).toEqual(expectedActions)
        })
    })
  });

  describe('fetchFlights', () => {
    it('creates SET_ERROR when fetchFlights fails due to invalid flightId', () => {
      const store = mockStore({ searchedFlights: {} })

      const testFlightId = "ABCDEF";
      var date = new Date();
      let day = date.getDate();
  	  let month = date.getMonth() + 1;
  	  let year = date.getFullYear();
      const testArrivalDate = year + " " + month + " " + day;

    	var fetchMock = require('fetch-mock');

    	// Mock the fetch() global to always return the same value for GET
    	// requests to all URLs.
    	fetchMock.get('*', {});
    	// Unmock.

    	const expectedActions = [
          { 
          	type: types.SET_FETCH_ERROR,
          	errorMessage: {
          		errorMessage: "ABCDEF is not a valid flight id."
          	} 
          }
      ]

      return store.dispatch(actions.fetchFlights(testFlightId, testArrivalDate))
        .then(() => { // return of async actions
          expect(store.getActions()).toEqual(expectedActions)

          fetchMock.restore();
        })
    })

    it('creates SET_ERROR when fetchFlights fails due to flight not found', () => {
      const store = mockStore({ searchedFlights: {} })

      const testFlightId = "AA210";
      var date = new Date();
      let day = date.getDate();
    	let month = date.getMonth() + 1;
    	let year = date.getFullYear();
      const testArrivalDate = year + " " + month + " " + day;

  	  var fetchMock = require('fetch-mock');

    	// Mock the fetch() global to always return the same value for GET
    	// requests to all URLs.
    	fetchMock.get('*', {});
    	// Unmock.

    	const expectedActions = [
          { 
          	type: types.SET_FETCH_ERROR,
          	errorMessage: {
          		errorMessage: `No flight found for: ${testFlightId}`
          	} 
          }
        ]

      return store.dispatch(actions.fetchFlights(testFlightId, testArrivalDate))
          .then(() => { // return of async actions
            expect(store.getActions()).toEqual(expectedActions)

            fetchMock.restore();
          })
    })

    it('creates SET_SEARCHED_FLIGHTS when fetchFlights is successful', () => {
      const store = mockStore({ searchedFlights: {} })

      const testFlightId = "AA210";
      var date = new Date();
      let day = date.getDate();
    	let month = date.getMonth() + 1;
    	let year = date.getFullYear();
        const testArrivalDate = year + " " + month + " " + day;

    	var fetchMock = require('fetch-mock');

    	fetchMock.get('*', {ok: true, body: testSuccessFlightStatusResponseMulti});

      const clearErrorAction = {
      	type: types.CLEAR_FETCH_ERROR
      };

      const setSearchedFlightsAction = {
      	type: types.SET_SEARCHED_FLIGHTS,
      	flightData: {}
      }

      return store.dispatch(actions.fetchFlights(testFlightId, testArrivalDate))
        .then(() => { // return of async actions
          expect(store.getActions()).toInclude(clearErrorAction);

  		expect(store.getActions()).toInclude(setSearchedFlightsAction, (first, second) => {
          	if (first.type == second.type && first.flightData ) {
          		return true;
          	} else {
          		return false;
          	}
          });

          fetchMock.restore();
        })
    })

    it('creates SET_SEARCHED_FLIGHTS when fetchFlights is successful', () => {
      const store = mockStore({ searchedFlights: {} })

      const testFlightId = "AA210";
      var date = new Date();
      let day = date.getDate();
    	let month = date.getMonth() + 1;
    	let year = date.getFullYear();
      const testArrivalDate = year + " " + month + " " + day;

  	 var fetchMock = require('fetch-mock');

  	 fetchMock.get('*', {ok: true, body: testSuccessFlightStatusResponseSingle});

      const clearErrorAction = {
      	type: types.CLEAR_FETCH_ERROR
      };

      const setSearchedFlightsAction = {
      	type: types.SET_SEARCHED_FLIGHTS,
      	flightData: {}
      }

      return store.dispatch(actions.fetchFlights(testFlightId, testArrivalDate))
        .then(() => { // return of async actions
          expect(store.getActions()).toInclude(clearErrorAction);

  		expect(store.getActions()).toInclude(setSearchedFlightsAction, (first, second) => {
          	if (first.type == second.type && first.flightData ) {
          		return true;
          	} else {
          		return false;
          	}
          });

          fetchMock.restore();
        })
    })

  });

})

describe('actions', () => {

	it('should create an action to set searched flights', () => {
		const flightData = {
			appendix: {},
			flightStatuses: [],
			request: {},
		}
		const expectedAction = {
			type: types.SET_SEARCHED_FLIGHTS,
			flightData,
		}
		expect(actions.setSearchedFlights({flightData: flightData})).toEqual(expectedAction);
	})	

	it('should create an action to set clear flights', () => {
		const expectedAction = {
			type: types.CLEAR_SEARCHED_FLIGHTS,
		}
		expect(actions.setClearFlights()).toEqual(expectedAction);
	})

	it('should create an action to clear error', () => {
		const expectedAction = {
			type: types.CLEAR_FETCH_ERROR,
		}
		expect(actions.clearError()).toEqual(expectedAction);
	})


	it('should create an action to set error', () => {
		const errorMessage = "This is a test error";
		const expectedAction = {
			type: types.SET_FETCH_ERROR,
			errorMessage
		}
		expect(actions.setError(errorMessage)).toEqual(expectedAction);
	})	


	it('should create an action to add flight', () => {
		const expectedAction = {
			type: types.ADD_FLIGHT,
		}
		expect(actions.addFlight()).toEqual(expectedAction);
	})	
})