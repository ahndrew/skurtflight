import React from 'react';
import {shallow} from 'enzyme'

import renderer from 'react-test-renderer';

import {AppContainer} from '../app/containers/AppContainer'
import {Provider} from 'react-redux';
import configureStore from '../index.android.js'

describe('components', function() {
  describe('<AppContainer />', function() {
    it('renders correctly', function() {
      const store = configureStore({});

      var tree = shallow(<Provider store={store} />);
      expect(tree).toMatchSnapshot();
    });
  });
});