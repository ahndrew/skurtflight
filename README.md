# README #

### Skurt Flight ###

* Sample project based on Skurt Reactive Programming Challenge
* This project allows a user to find their flight using their flight id and arrival date
* Version 1.0

### How do I get set up? ###

* Summary of set up
* npm install in root directory to get started

## Android ##
* connect android emulator / device
* react-native run-android in root directory

## iOS ##
* connect ios simulator / device
* react-native run-ios in root directory

### Libraries used ###
* react
* react-native
* react-native-datepicker
* react-redux
* redux
* redux-logger
* redux-tunk
* moment
* moment-timezone


### Api ###
https://developer.flightstats.com/api-docs/flightstatus/v2/flight

### Improvements ###
1.  add additional ways for a user to search for their flights
    (e.g. departure date, confirmation code, airline + dates etc)

2. selecting a flight currently only shows an alert
   this eventually would get hooked up to be saved so that a user
   can request a car to be delivered to their arrival airport

3. add additional flight information / add airline logos

4. clean up code and separate out some of the components

5. write tests

### Who do I talk to? ###

* Andrew (ahn.andrews@gmail.com)