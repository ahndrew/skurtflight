class FlightHelper {

	// these keys probably need to be moved elsewhere
	static appId = "91b929e6";
	static appKey = "2eebba75c50ce13c31b9ef0b331fb93a";
	static path = "/flex/flightstatus/rest/v2/json/flight/status/";

	static isFlightStatsId(flightId) {
		let pattern = new RegExp(/\s*^[0-9]*$\s*$/);
		return pattern.test(flightId);
	}

	static parseFlightStatsId(flightId) {
		var flightInfo = flightId.match(/\s*(^[0-9]*$)\s*$/);
		if (flightInfo && flightInfo.length > 0) {
			return flightInfo[1];
		} else {
			return null;
		}
	}

	static isValidFlight(flightId) {
		let pattern = new RegExp(/[a-zA-Z]+[\s]*[0-9]+\s*$/);
		return pattern.test(flightId);
	}

	static parseFlightId(flightId) {
		var flightInfo = flightId.match(/([a-zA-Z]+)[\s]*([0-9]+)/);
		if (flightInfo && flightInfo.length > 1) {
			return { airlineCode : flightInfo[1], flightId: flightInfo[2] };
		} else {
			return {};
		}
	}

	static getApiKeyParams() {
		return params = [
			`appId=${this.appId}`,
			`appKey=${this.appKey}`,
		].join('&');
	}

	static getFlightStatusUrl(flightId, arrivalDate) {
		var url = this.path;

		if (FlightHelper.isFlightStatsId(flightId) && !FlightHelper.isValidFlight(flightId)) {
			url += FlightHelper.parseFlightStatsId(flightId);
		} else if (FlightHelper.isValidFlight(flightId)) {
			var fullFlightId = FlightHelper.parseFlightId(flightId);

			// append airline abbreviation
    		var airline = fullFlightId.airlineCode;

     		// append flight #
     		var flight = fullFlightId.flightId;

     		url += airline + "/" + flight + "/";

     		var date = new Date();

			if (arrivalDate) {				
				date = new Date(arrivalDate);
			}

			let day = date.getDate();
	    	let month = date.getMonth() + 1;
	    	let year = date.getFullYear();
	    		
			url += "arr/" + year + "/" + month + "/" + day;

		}

		return url;
	}

	/*
	* Adds airline info to flight from appendix and local date for arrival/departure
	*
	* May want to modify this to parse out extraneous flight info as well
	*/
	static addAdditionalFlightData(appendix, flight) {
		
		if (appendix == null || appendix == undefined) {
			return flight;
		} else if (flight == null || flight == undefined) {
			return undefined;
		}

		var moment = require('moment-timezone');

		// airline info
		var airlineInfo = appendix.airlines.find((airline) => {
			return flight.carrierFsCode == airline.fs;
		});

		flight.flightDisplayName = airlineInfo.name + " " + flight.flightNumber;

		// departure airport info
		var departureAirportInfo = appendix.airports.find((airport) => {
			return flight.departureAirportFsCode == airport.fs;
		});

		flight.departureAirportDisplay = flight.departureAirportFsCode + " - " 
				+ departureAirportInfo.city + ", " 
				+ (departureAirportInfo.stateCode ? 
					departureAirportInfo.stateCode : 
					departureAirportInfo.countryCode)

		// arrival airport info
		var arrivalAirportInfo = appendix.airports.find((airport) => {
			return flight.arrivalAirportFsCode == airport.fs;
		});

		flight.arrivalAirportDisplay = flight.arrivalAirportFsCode + " - " 
				+ arrivalAirportInfo.city + ", " 
				+ (arrivalAirportInfo.stateCode ? 
					arrivalAirportInfo.stateCode : 
					arrivalAirportInfo.countryCode);


		var departureMoment = moment.parseZone(flight.departureDate.dateLocal);
		var arrivalMoment = moment.parseZone(flight.arrivalDate.dateLocal);
		
		flight.departureLocalDateDisplay = departureMoment.format("dddd M/D/YYYY - hh:mm A") + " "
								 + moment.tz(departureAirportInfo.timeZoneRegionName).zoneAbbr();

		flight.arrivalLocalDateDisplay = arrivalMoment.format("dddd M/D/YYYY - hh:mm A") + " "
								 + moment.tz(arrivalAirportInfo.timeZoneRegionName).zoneAbbr();											 

		return flight;
	}
}
export default FlightHelper