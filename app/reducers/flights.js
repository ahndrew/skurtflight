import createReducer from '../lib/createReducer'
import * as types from '../actions/types'
import FlightHelper from '../lib/flightHelper'

export const searchedFlights = createReducer({}, {
	[types.SET_SEARCHED_FLIGHTS](state, action) {
		let newState = {};
		
		if (action.flightData) {
			if (action.flightData.flightStatus) {

				newState[action.flightData.flightStatus.flightId] = 
					FlightHelper.addAdditionalFlightData(action.flightData.appendix, 
						action.flightData.flightStatus);

				return newState;
			} else if (action.flightData.flightStatuses) {

				action.flightData.flightStatuses.forEach( (flight) => {

					newState[flight.flightId] = 
						FlightHelper.addAdditionalFlightData(action.flightData.appendix, flight);

				});

				return newState;
			}
		} 

		return state;
	},
	[types.CLEAR_SEARCHED_FLIGHTS](state, action) {
		return {};
	}
});

export const fetchError = createReducer({}, {
	[types.SET_FETCH_ERROR](state, action) {
		let newState = {};

		if (action.errorMessage) {
			newState = action.errorMessage;
		}

		return newState;
	},
	[types.CLEAR_FETCH_ERROR](state, action) {
		let newState = {};

		return newState;
	}
})