import { combineReducers } from 'redux';
import * as flightsReducer from './flights';

export default combineReducers(Object.assign(
	flightsReducer,
));