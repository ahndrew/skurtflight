import React, { Component } from 'react'
import PropTypes from 'prop-types';
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import DatePicker from 'react-native-datepicker'

const {
	ScrollView,
	View,
	Text,
	TextInput,
	Image,
	TouchableHighlight,
	StyleSheet,
	Navigator,
	TouchableOpacity,
	Animated,
	Easing,
	Alert
} = ReactNative

export class Home extends Component {

	constructor(props) {
		super(props)

		var moment = require('moment-timezone');

		var minDate = moment();
		var maxDate = moment();
		maxDate.add(3, 'years');

  		this.minDateFormatted = minDate.format("MM/DD/YYYY");
  		this.maxDateFormatted = maxDate.format("MM/DD/YYYY");		

		this.state = { searching: false, 
						flightInput: '', 
						errorMessage: '', 
						flightRowId: '',
						spinValue: new Animated.Value(0),
						arrivalDate: '',
						minDateFormatted: this.minDateFormatted,
						maxDateFormatted: this.maxDateFormatted };
	}

	componentDidMount () {
	  this.spin()
	}

	spin () {
	  this.state.spinValue.setValue(0)
	  Animated.timing(
	    this.state.spinValue,
	    {
	      toValue: 1,
	      duration: 2000,
	      easing: Easing.linear
	    }
	  ).start(() => this.spin())
	}

	searchedPressed() {
		this.setState({ searching: true, errorMessage : '', flightRowId: '' })
		this.props.fetchFlights(this.state.flightInput, this.state.arrivalDate).then( () => {	
			this.setState({ searching: false });
			this.setState({ errorMessage: this.errorMessage() })
		});
	}

	flights() {
		return Object.keys(this.props.searchedFlights).map( key => this.props.searchedFlights[key] );
	}

	errorMessage() {
		if (this.props.fetchError) {
			return this.props.fetchError.errorMessage;
		} else {
			return "";
		}
	}

	selectPressed(flightRowId, flightDisplayName) {
		this.setState({ flightRowId: flightRowId});
		Alert.alert(
			  "You've selected a flight.",
			  flightDisplayName,
			  [
			    {text: 'OK', onPress: () => console.log('OK Pressed')},
			  ],
			  { cancelable: false }
			);
	}

	onArrivalDatePickerPressed = (component) => {
		this.arrivalDatePicker.onPressDate();
	}

	clearInput = () => {
		this.flightIdInput.value = '';
		this.arrivalDatePicker.date = '';
		this.setState({flightInput: '', arrivalDate: ''});
		this.props.clearFlights();
	}

	flightIdFocus = () => {
		this.flightIdInput.focus();
	}

	render() {
		const routes = [
    		{title: 'First Scene', index: 0},
  		];
		
		const spin = this.state.spinValue.interpolate({
    		inputRange: [0, 1],
    		outputRange: ['0deg', '360deg']
  		});

		return (
		    <Navigator
		      renderScene={(route, navigator) =>
		        <View style={styles.scene}>
		        	<View style={styles.sceneTitleContainer}>
		        		<Text style={styles.sceneTitle}>FIND YOUR FLIGHT</Text>

		        		<TouchableHighlight 
		        			style={styles.clearButton}
		        			underlayColor="#EEE"
		        			onPress={() => this.clearInput()}>
		        			<Text style={styles.clearButtonText}>Clear</Text>
		        		</TouchableHighlight>
		        	</View>
		        	<View style={styles.flightIdSection}>

		        		<TouchableHighlight 
		        			style={styles.flightIdEntrySection}
		        			underlayColor="#EEE"
		        			onPress={() => this.flightIdFocus()}>
		        		<View>
			        		<Text style={styles.flightNumText}>Flight #</Text>
			        		<TextInput style={styles.searchInput}
			        			autoCorrect={false}
			        			underlineColorAndroid='transparent'
								returnKeyType='search'
								returnKeyLabel='search'
								placeholder='Enter Flight # (e.g. AA210)'
								placeholderTextColor='#CACACA'
								onChangeText={ (flightInput) => this.setState({flightInput}) }
								value={this.state.flightInput}
								onSubmitEditing={() => this.searchedPressed() }
								ref={ (component) => this.flightIdInput = component }
							/>
						</View>
						</TouchableHighlight>
		        	</View>
		        	<View style={styles.datesRow}>
		        		<TouchableHighlight
        					style={styles.dateSection}
        					underlayColor="#EEE"
        					onPress={() => this.onArrivalDatePickerPressed()}>
        					<View>
			        			<Text>Arrival Date</Text>
			        			<DatePicker
							        style={{width: 100, height: 20}}
							        date={this.state.arrivalDate}
							        mode="date"
							        placeholder="Tap to Select"
							        format="MM/DD/YYYY"
							        minDate={this.state.minDateFormatted}
							        maxDate={this.state.maxDateFormatted}
							        confirmBtnText="Confirm"
							        cancelBtnText="Cancel"
							        showIcon={false}
							        customStyles={datePickerStyle}
							        onDateChange={(date) => {this.setState({arrivalDate: date})}}
							        ref={ (component) => this.arrivalDatePicker = component }
							      />
						    </View>
		        		</TouchableHighlight>
		        	</View>
		        	<View style={styles.sceneTitleContainer}>
		        		<Text style={styles.sceneTitle}>FLIGHT LIST</Text>
		        	</View>
		        	
		        	{!this.state.searching && !this.state.errorMessage && this.flights() ?

						<ScrollView style={styles.scrollSection}>

						{!this.state.searching && !this.state.errorMessage && this.flights().map((flight) => {
							return <View key={flight.flightId} style={styles.flightContainer} 
										backgroundColor={this.state.flightRowId == flight.flightId ? "#EEE" : "#FFF"}>
										
										<View style={styles.flightContainerLeft}>

											<View style={styles.flightInfoContainer}>
												<Text style={styles.flightInfo}>{flight.flightDisplayName}</Text>
											</View>
											<View style={styles.flightAirportContainer}>
												<Text style={styles.airportInfoTitle}>Departing:</Text>
												<View style={styles.airportInfoContainer}>
													<Text style={styles.airportDisplayText}>
													{flight.departureAirportDisplay}
													</Text>
													<Text style={styles.airportDisplayText}>
													{flight.departureLocalDateDisplay}
													</Text>
												</View>
											</View>
											<View style={styles.flightAirportContainer}>
												<Text style={styles.airportInfoTitle}>Arriving:</Text>
												<View style={styles.airportInfoContainer}>
													<Text style={styles.airportDisplayText}>
													{flight.arrivalAirportDisplay}
													</Text>
													<Text style={styles.airportDisplayText}>
													{flight.arrivalLocalDateDisplay}
													</Text>
												</View>
											</View>
										</View>

										<TouchableOpacity onPress={() => this.selectPressed(flight.flightId, flight.flightDisplayName)}
											style={styles.selectFlightButton}>
											<Text style={styles.selectFlightButtonText}>SELECT FLIGHT</Text>
										</TouchableOpacity>
									</View>
						})}

						</ScrollView>
						: null 
					}
						

					{ this.state.searching && !this.state.errorMessage ?
						<View style={styles.searchingContainer}>
							<Animated.Image                         // Base: Image, Text, View
	        					source={require('../../img/airplane.png')}
	        					style={{
	        						height: 50,
									width: 50,
									marginBottom: 10,
	          						transform: [{rotate: spin}],
	        					}} />
							<Text>Searching...</Text> 
						</View>	
						: null
					}
					
					{ !this.state.searching && this.state.errorMessage ?
						<View style={styles.errorMessageContainer}>
							<Text>{this.state.errorMessage}</Text> 
						</View>
						: null
					}					

					<TouchableOpacity onPress={() => this.searchedPressed() } 
						underlayColor="#CACACA"
						style={styles.searchButton} >
						<Text style={styles.searchButtonText}>Find Flight</Text>
					</TouchableOpacity>
				</View>
		      }
		      navigationBar={
			     <Navigator.NavigationBar
			       navigationStyles={Navigator.NavigationBar.StylesIOS}
			       routeMapper={{
			       		LeftButton: (route, navigator, index, navState) => { return; },
			        	RightButton: (route, navigator, index, navState) => { return; },
			        	Title: (route, navigator, index, navState) => { 
			        		return (
			           			<Image source={require('../../img/p_m_skurt-logo.png')}
			           				style={styles.headerImage} />
			           		); 
			       		},
			       	}}
			        style={styles.navBar}
			     />
		  		}
		    />
		 );
	}
}

Home.propTypes = {
  
};

const datePickerStyle = StyleSheet.create({
	dateTouchBody: {
		height: 20,
	},	
	dateInput: {
		marginLeft: 0,
		borderWidth: 0,
		height: 20,
		alignItems: 'flex-start'
	},
	dateText: {
		fontSize: 12,
		color: "#939292"
	},
	placeholderText: {
		fontSize: 12,
	},
})

const styles = StyleSheet.create({
	scene: {
		flex: 1,
		marginTop: 70,
	},

	navBar: {
		flex: 1,
		borderBottomColor: '#CACACA',
		borderBottomWidth: 0.5,
		height:70,
	},

	headerImage: {
		flex:1,
		height: 13,
		width: 105,
		resizeMode: 'contain',
	},

	sceneTitleContainer: {
		height: 35,
		borderBottomWidth: 0.5,
		borderBottomColor: '#CACACA',
		paddingTop: 5,
		paddingBottom: 5,
		paddingLeft: 20,
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'space-between'
	},

	sceneTitle: {
		color: "#4990E2",
		fontSize: 12,
	},

	clearButton: {
		marginRight: 20,
		padding: 5,
	},

	clearButtonText: {
		fontSize: 10,
	},

	flightIdSection: {
		height: 60,
		flexDirection: 'row',
		borderBottomWidth: 0.5,
		borderBottomColor: '#CACACA',
		marginLeft: 0.5,
		marginRight: 0.5,
	},

	flightIdEntrySection: {
		flex: 0.7,
		paddingLeft: 20,
		paddingTop: 5,
		paddingBottom: 5,
		paddingRight: 20,
		justifyContent: 'center',
	},

	flightNumText: {
		height: 20,
	},

	searchInput: {
		paddingTop: 2,
		paddingBottom: 2,
		height: 30,
		fontSize: 12,
	},

	datesRow: {
		flexDirection: 'row',
		height: 50,
		borderBottomWidth: 0.5,
		borderBottomColor: '#CACACA',
		marginLeft: 0.5,
		marginRight: 0.5,
	},

	dateSection: {
		flex: 0.5,
		height: 50,
		borderLeftWidth: 0.5,
		borderLeftColor: '#CACACA',
		paddingLeft: 20,
		paddingTop: 5,
		paddingBottom: 5,
		paddingRight: 20,
		justifyContent: 'center',
	},

	dateStyle: {
		color: '#CACACA',
		fontSize: 12,
	},



	scrollSection: {
		
	},

	flightContainer: {
		height: 150,
		borderWidth: 0.5,
		borderColor: '#CACACA',
		paddingLeft: 20,
		paddingTop: 5,
		paddingBottom: 5,
		flexDirection: 'row',
		alignItems: 'center'
	},

	flightContainerLeft: {
		flex: 0.7,
	},

	selectFlightButton: {
		flex: 0.3,
		backgroundColor: "#4990E2",
		marginRight: 20,
		height: 25,
		alignItems: 'center',
		justifyContent: 'center',
	},

	selectFlightButtonText: {
		fontSize: 10,
		color: "#FFF",
	},

	flightInfoContainer: {
		height: 30,
		paddingTop: 5,
		paddingBottom: 20,
	},

	flightInfo: {
		color: '#4990E2',
		height: 30,
		paddingBottom: 15,
	},

	flightAirportContainer: {
		height: 50,
	},

	airportInfoTitle: {
		fontSize: 10,
	},

	airportInfoContainer: {
		paddingLeft: 20,
	},

	airportDisplayText: {
		fontSize: 10,
	},


	searchButton: {
		height: 50,
		backgroundColor: '#63407D',
		alignItems:'center',
        justifyContent: 'center',
	},

	searchButtonText: {
		color: '#FFF',
	},

	searchingContainer: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},

	errorMessageContainer: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},

	airplaneImage: {
		height: 50,
		width: 50,
		marginBottom: 10,
	}
});

function mapStateToProps(state) {
	return {
		searchedFlights: state.searchedFlights,
		fetchError: state.fetchError
	}
}

export default connect(mapStateToProps)(Home);