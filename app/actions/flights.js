import * as types from './types'
import Api from '../lib/api'
import FlightHelper from '../lib/flightHelper'

export function fetchFlights(flightId, arrivalDate) {

	return (dispatch, getState) => {

		if (!FlightHelper.isValidFlight(flightId) && !FlightHelper.isFlightStatsId(flightId)) {
			dispatch(setError({ errorMessage: flightId ? flightId + " is not a valid flight id." : "Not a valid flight id"}));
			return Promise.resolve();
		}

		let params = FlightHelper.getApiKeyParams();
		let url = FlightHelper.getFlightStatusUrl(flightId, arrivalDate);

		return Api.get(`${url}?${params}`).then(resp => {
			if (resp.flightStatus) {
				dispatch(clearError());
				dispatch(setSearchedFlights({ flightData: resp }));
			} else if (resp.flightStatuses && resp.flightStatuses.length > 0) {
				dispatch(clearError());
				dispatch(setSearchedFlights({ flightData: resp }));
			} else {
				dispatch(setError({ errorMessage: "No flight found for: " + flightId}));
			}
			
		}).catch( (ex) => {
			
			dispatch(setError({ errorMessage: flightId ? flightId + " is not a valid flight id." : "Not a valid flight id"}));

		})
	}
}

export function setSearchedFlights( { flightData } ) {
	return {
		type: types.SET_SEARCHED_FLIGHTS,
		flightData
	}
}

export function clearFlights() {
	return (dispatch, getState) => {
		dispatch(setClearFlights());

		return Promise.resolve();
	}
}

export function setClearFlights() {
	return {
		type: types.CLEAR_SEARCHED_FLIGHTS,
	}
}

export function clearError() {
	return {
		type: types.CLEAR_FETCH_ERROR,
	}
}

export function setError(errorMessage) {
	return {
		type: types.SET_FETCH_ERROR,
		errorMessage
	}
}

export function addFlight() {
	return {
		type: types.ADD_FLIGHT,
	}
}