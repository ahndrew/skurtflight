import * as FlightActions from './flights';

export const ActionCreators = Object.assign({},
	FlightActions,
);
